<?php namespace App\Http\Controllers;
// allow origin
header('Access-Control-Allow-Origin: *');
// add any additional headers you need to support here
header('Access-Control-Allow-Headers: Origin, Content-Type');
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Offerts;
use App\User;

class OffertController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		try{


			$error=false;

			//show a list of offerts
			if(Offerts::count("id")==0)
		  		$msg = "No se Consiguieron Resultados";
		  	else	  		
		  		$msg = "Ok";



	 	 }catch(\Exception $e){

	 	 	$error=true;
			$msg=$e->getMessage();


	 	 }finally{

	 	 	 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>Offerts::orderBy("id","desc")->limit(0,20)->get()

      			), 200);

	 	 }
 	
			
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//adding an new offert
		$user=array();
		try{
			$this->validarCampos($request,"nuevo");

			$msg="Registrado correctamente";
			$error=false;
			$offert=new Offerts;
			$offert->ciudad=$request->ciudad;
			$offert->death_line=$request->death_line;
			$offert->presupuesto=$request->presupuesto;
			$offert->descripcion=$request->descripcion;
			$offert->id_user=$request->id_user;

			$offert->save();
			

		}catch(\Exception $e){

			$error=true;
			$msg=$e->getMessage();

		}finally{

			 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>$user

      			), 200);

		}
	}






	public function validarCampos($request,$accion){

	
		if(trim($request->ciudad)=="")
			throw new \Exception("Ciudad no puede estar vacio", 1);
		else
		if(trim($request->death_line)=="")
			throw new \Exception("Death Line no puede estar vacio", 1);
		else
		if(trim($request->presupuesto)=="")
			throw new \Exception("Presupuesto no puede estar vacio", 1);
		else
		if(trim($request->descripcion)=="")
			throw new \Exception("descripcion no puede estar vacio", 1);	
		else
		if(trim($request->id_user)=="")
			throw new \Exception("Usuario no puede estar vacio", 1);
		else
		if(User::where("id","=",$request->id_user)->count("id")==0)
			throw new \Exception("Usuario no existe", 1);	

		
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
