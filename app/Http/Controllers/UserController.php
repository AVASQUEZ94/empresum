<?php namespace App\Http\Controllers;
// allow origin
header('Access-Control-Allow-Origin: *');
// add any additional headers you need to support here
header('Access-Control-Allow-Headers: Origin, Content-Type');
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use Auth;
class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		//get info to store the user
		$user=array();
		try{
			$this->validarCampos($request,"nuevo");

			$msg="Registrado correctamente";
			$error=false;
			$user = new User;
			$user ->nombre = $request->nombre;
			$user ->nit = $request->nit;
			$user ->sector_industrial = $request->sector_industrial;
			$user ->password = $request->password;
			$user ->email = $request->email;
			$user ->responsable = $request->responsable;
			$user ->cargo = $request->cargo;
			$user ->pagina_web = $request->pagina_web;
			$user ->celular = $request->celular;
			$user ->telefono = $request->telefono;
			$user ->direccion = $request->direccion;
			$user ->ciudad = $request->ciudad;
			$user ->pais = $request->pais;

			$user->save();
			

		}catch(\Exception $e){

			$error=true;
			$msg=$e->getMessage();

		}finally{

			 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>$user

      			), 200);

		}
	}





	public function validarCampos($request,$accion){

		if($accion=="actualizar"){

			if(trim($request->id)=="")
				throw new \Exception("Id no puede estar vacio", 1);
			else
			if(User::where("id","=",$request->id)->count("id")==0)
				throw new \Exception("Usuario no existe", 1);
		}





		if(trim($request->email)=="")
			throw new \Exception("Email no puede estar vacio", 1);
		else
		if(trim($request->nit)=="")
			throw new \Exception("NIT no puede estar vacio", 1);
		else
		if(trim($request->password)=="")
			throw new \Exception("La contraseña no puede estar vacio", 1);
		else
		if(trim($request->nombre)=="")
			throw new \Exception("Nombre no puede estar vacio", 1);
		else
		if(User::where("email","=",$request->email)->count("id")>0)
			throw new \Exception("Email Duplicado", 1);
		else
			if(User::where("nit","=",$request->nit)->count("id")>0)				
				throw new \Exception("NIT Duplicado", 2);



		
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request)
	{
		//get info to edit the user
		$user=array();
		try{
			$msg="Ok";
			$error=false;
			if(User::where("id","=",$request->id)->count("id")==0){
				$msg="No se consiguieron Resultados";
				$error=true;
			}
			else
				$user=User::findOrFail($request->id);
			

		}catch(\Exception $e){

			$error=true;
			$msg=$e->getMessage();

		}finally{

			 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>$user

      			), 200);

		}


	}

	public function darmeBaja(Request $request){


		

		$user=array();
		try{
			$msg="Ok";
			$error=false;
			if(User::where("id","=",$request->id)->count("id")==0){
				$msg="No se consiguieron Resultados";
				$error=true;
			}
			else
				{
					$user=User::find($request->id);
					$user->status=0;
					$user->save();
					$msg="Usuario Inactivado correctamente";
				}
			

		}catch(\Exception $e){

			$error=true;
			$msg=$e->getMessage();

		}finally{

			 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>$user

      			), 200);

		}

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
				
			//get info to edit the user
		$user=array();
		try{
			$this->validarCampos($request,"actualizar");

			$msg="Modificado correctamente";
			$error=false;
			$user=User::findOrFail($request->id);		
			$user ->nombre = $request->nombre;
			$user ->nit = $request->nit;
			$user ->sector_industrial = $request->sector_industrial;
			$user ->password = $request->password;
			$user ->email = $request->email;
			$user ->responsable = $request->responsable;
			$user ->cargo = $request->cargo;
			$user ->pagina_web = $request->pagina_web;
			$user ->celular = $request->celular;
			$user ->telefono = $request->telefono;
			$user ->direccion = $request->direccion;
			$user ->ciudad = $request->ciudad;
			$user ->pais = $request->pais;
			$user ->descripcion = $request->descripcion;
			$user ->clientes = $request->clientes;
			$user ->redes_sociales = $request->redes_sociales;

			$user->save();
				
			}catch(\Exception $e){
				$error=true;
				$msg=$e->getMessage();
			}finally{

			return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>$user

      			), 200);
			}

	
	}


	public function aliados(){
		//show a list of requeriments


		try{


			$error=false;

			//show a list of offerts
			if(User::count("id")==0)
		  		$msg = "No se Consiguieron Resultados";
		  	else	  		
		  		$msg = "Ok";



	 	 }catch(\Exception $e){

	 	 	$error=true;
			$msg=$e->getMessage();


	 	 }finally{

	 	 	 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>User::orderBy("id","desc")->limit(0,20)->get()

      			), 200);

	 	 }
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
