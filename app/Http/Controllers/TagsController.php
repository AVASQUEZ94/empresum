<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Tags;
use Auth;
use App\User;
use App\UserTags;
class TagsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		//get tags

		$user=array();
		try{

			$error=false;
			if(Tags::count("id")==0)
			$msg="No se consiguieron resultados";
			else
			$msg="Ok";

			$user=Tags::select("id","descripcion")->get();
			

		}catch(\Exception $e){

			$error=true;
			$msg=$e->getMessage();

		}finally{

			 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>$user

      			), 200);

		}


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//get info to store the user
		$user=array();
		try{
			$this->validarCampos($request,"nuevo");

			$msg="Registrado correctamente";
			$error=false;
			$tags = new UserTags;
			$tags ->id_tag = $request->id_tag;
			$tags ->id_user = $request->id_user;
			$tags ->tipo = $request->tipo;
		
			$tags->save();
			

		}catch(\Exception $e){

			$error=true;
			$msg=$e->getMessage();

		}finally{

			 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>$user

      			), 200);

		}
	}
	public function validarCampos($request,$accion){

	
		if(trim($request->id_tag)=="")
			throw new \Exception("Etiqueta no puede estar vacio", 1);
		else
		if(trim($request->id_user)=="")
			throw new \Exception("Usuario no puede estar vacio", 1);
		else
		if(trim($request->tipo)<1 || trim($request->tipo)>2 )
			throw new \Exception("Tipo invalido", 1);	
		else
		if(User::where("id","=",$request->id_user)->count("id")==0)
			throw new \Exception("Usuario no existe", 1);	
		else
		if(Tags::where("id","=",$request->id_tag)->count("id")==0)
			throw new \Exception("Etiqueta no existe", 1);	
		else
		if(UserTags::where("id_tag","=",$request->id_tag)->where("id_user","=",$request->id_user)->count("id")>0)
			throw new \Exception("Etiqueta ya registrada para esta empresa", 1);	
			
	}






	public function traerEtiquetasPorUsuario(Request $request){

			//get tags by user

		$tags=array();
		try{

			$error=false;
			if(UserTags::where("id_user","=",$request->id_user)->where("tipo","=",$request->tipo)->count("id")==0)
			$msg="No se consiguieron etiquetas para de este tipo y usuario";
			else
			$msg="Ok";

			$tags=Tags::whereIn("id",UserTags::select("id_tag")->where("id_user","=",$request->id_user)->where("tipo","=",$request->tipo)->lists("id_tag"))->get();
			

		}catch(\Exception $e){

			$error=true;
			$msg=$e->getMessage();

		}finally{

			 return response()->json(

      			array(
      				'error' => $error,
      				'msg'=> $msg  ,
      			'resultado'=>$tags

      			), 200);

		}

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
