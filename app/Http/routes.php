<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');


#ofertas
Route::group(['prefix' => 'ofertas'],function (){

	Route::post('offerts', 'OffertController@index');

	Route::post('/registro', 'OffertController@store');

});


#requerimientos
Route::group(['prefix' => 'requerimiento'],function (){

	Route::post('requeriments', 'RequerimentController@index');

	Route::post('/registro', 'RequerimentController@store');

});

#usuarios
Route::group(['prefix' => 'usuarios'],function (){

	Route::post('/datosUser', 'UserController@edit');

	Route::post('/registro', 'UserController@store');

	Route::post('/editarDatos', 'UserController@update');

	Route::post('/darmeBaja', 'UserController@darmeBaja');

	Route::post('/traerEtiquetas', 'TagsController@traerEtiquetasPorUsuario');

});



Route::post('aliados', 'UserController@aliados');

Route::post('login/usuarios', 'LoginController@store');


#tags
Route::group(['prefix' => 'tags'],function (){

	Route::post('tags', 'TagsController@index');

	Route::post('/registro', 'TagsController@store');

});



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
