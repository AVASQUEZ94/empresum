<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['nombre', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	public function Actualizar($user,$request){

				$user->name=$request->nombre;
				$user->nit=$request->nit;
				$user->sector=$request->nit;
				$user->responsable=$request->nit;
				$user->cargo=$request->nit;
				$user->telefono=$request->nit;
				$user->direccion=$request->nit;
				$user->celular=$request->nit;
				$user->ciudad=$request->nit;
				$user->pais=$request->nit;
				$user->descripcion=$request->nit;
				$user->clientes=$request->nit;
				$user->sitio_web=$request->nit;
				$user->redes_sociales=$request->nit;
			
				return	$user->save();
	}

}
